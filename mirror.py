#!/usr/bin/env python

import sys, os, fcntl, signal
from rfc822 import Message

class MailHandler(object):
    def __init__(self):
        self.maildir = "/home/x469yq/Maildir"
        self.new_dir = os.path.join(self.maildir, "new")
        self.cur_dir = os.path.join(self.maildir, "cur")

        self.queue = []

        self.probe()

    def connect(self):
        fd = os.open(self.new_dir, os.O_RDONLY)
        fcntl.fcntl(fd, fcntl.F_NOTIFY,fcntl.DN_CREATE)
        signal.signal(signal.SIGIO, self.on_event)

    def on_event(self, signum, frame):
        self.probe()
        self.connect()

    def probe(self):
        for f in os.listdir(self.new_dir):
           path = os.path.join(self.new_dir, f)
           new_path = os.path.join(self.cur_dir, f)
           if os.path.isfile(path):
               self.parse(path)
               try:
                   os.rename(path, new_path)
               except:
                   pass
        self.empty_queue()
        print "queue be empty"

    def parse(self, path):
        f = open(path)
        m = Message(f)
        module = m.getheader("X-Topics")
        if module:
            if module not in self.queue:
                self.queue.append(module)

    def empty_queue(self):
        for module in self.queue:
            for vcs in ("svn", "bzr", "git", "hg"):
                self.update(vcs, module)
        self.queue = []

    def update(self, vcs, module):
        moddir = os.path.join("/srv", vcs, module)
        if os.path.isdir(moddir):
            self.run("%s-update.sh" % vcs, module)
        else:
            self.run("%s-create.sh" % vcs, module)

    def run(self, command, module):
        os.system("%s %s" % (os.path.join(os.path.expanduser("~"), command), module))

if __name__ == "__main__":
    x = MailHandler()
    if "--once" not in sys.argv:
        x.connect()
        while True:
            try:
                signal.pause() # sleep until signal received
            except KeyboardInterrupt:
                sys.exit(0)

