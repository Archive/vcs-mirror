#!/bin/sh
MODULE=$1
UUID=$(svn info http://svn.gnome.org/svn/$MODULE | grep 'Repository UUID: ' | sed 's/Repository UUID\: //')

echo $MODULE $UUID

cd /srv/svn

# Create a new repository
svnadmin create $MODULE

# Force UUID to match old repository
cat - <<EOF | svnadmin load --force-uuid $MODULE
SVN-fs-dump-format-version: 2

UUID: $UUID
EOF

# svnsync hooks
rm -rf $MODULE/hooks
mkdir $MODULE/hooks
echo '#!/bin/sh' > $MODULE/hooks/pre-revprop-change
chmod +x $MODULE/hooks/pre-revprop-change

# Set up svnsync
svnsync init file:///srv/svn/$MODULE http://svn.gnome.org/svn/$MODULE
svnsync sync file:///srv/svn/$MODULE
