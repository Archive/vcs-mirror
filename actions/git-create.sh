#!/bin/sh
echo $1

# Make a directory for our mirror
mkdir -p /srv/git/$1
cd /srv/git/$1

# We want naked mirrors
GIT_DIR=. ; export GIT_DIR

# Create a new repo and pull in the GNOME module
git init
git svn init --stdlayout --rewrite-root svn+ssh://svn.gnome.org/svn/$1 file:///srv/svn/$1
git svn fetch

# The packing isn't very agressive right now, so repack it like hell
git gc --prune --aggressive
git repack -adf --window=100

# Point master at svn/trunk
git symbolic-ref refs/heads/master refs/remotes/trunk
