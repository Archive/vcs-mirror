#!/bin/sh
MODULE=$1

bzr svn-import --incremental file:///srv/svn/$MODULE /srv/bzr/$MODULE

# make trunk searchable
bzr index $MODULE/trunk
