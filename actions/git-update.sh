#!/bin/sh

echo $1
cd /srv/git/$1

#
# Pull the latest from svn.gnome.org
#
git --bare svn fetch

#
# Update .git/refs/{heads|tags}/svn/* from refs/remotes/*
#
SET_ECHO="set -x" # enable echo
SET_ECHO="set +x" # disable echo

# update all branches and tags
git for-each-ref "--format=%(refname)" refs/remotes |
  sed 's,/\([^/]*\)$, \1,' | while read trunk ref ; do
  	case "$trunk" in
	*/tags)	($SET_ECHO ; git update-ref refs/tags/svn/$ref  $trunk/$ref ) ;;
	*)	($SET_ECHO ; git update-ref refs/heads/svn/$ref $trunk/$ref ) ;;
	esac
done

# delete stale branches
git for-each-ref "--format=%(refname)" refs/heads/svn |
  sed 's,refs/heads/svn/,,' | while read ref ; do
  	git rev-parse "refs/remotes/$ref" >/dev/null 2>&1 ||
		($SET_ECHO ; git update-ref -d "refs/heads/svn/$ref" "refs/heads/svn/$ref" )
done

# delete stale tags
git for-each-ref "--format=%(refname)" refs/tags/svn |
  sed 's,refs/tags/svn/,,' | while read ref ; do
  	git rev-parse "refs/remotes/tags/$ref" >/dev/null 2>&1 ||
		($SET_ECHO ; git update-ref -d "refs/tags/svn/$ref" "refs/tags/svn/$ref" )
done

#
# Allow dumb transports to work
#
exec git-update-server-info
