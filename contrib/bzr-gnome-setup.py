#!/usr/bin/env python

import os
import pwd
import subprocess
import sys

from bzrlib import bzrdir
from bzrlib.branch import Branch
from bzrlib.config import LocationConfig, STORE_LOCATION_APPENDPATH

REPO_BASE_DIR = os.path.expanduser('~/gnome-bzr/repos')
WORKINGTREE_BASE_DIR = os.path.expanduser('~/gnome-bzr/src')
USER = os.environ.get('GNOME_USER', pwd.getpwuid(os.getuid()).pw_name)
HOST = os.environ.get('GNOME_BZR_PLAYGROUND_HOST', 'bzr-playground.gnome.org')

if len(sys.argv) == 2:
    project = sys.argv[1]
    branch_name = 'devel'
elif len(sys.argv) == 3:
    project = sys.argv[1]
    branch_name = sys.argv[2]
else:
    sys.stderr.write('usage: %s project [branch-name]\n' % sys.argv[0])
    sys.exit(1)

remote_branch = 'bzr+ssh://%s/bzr/gnome/%s/trunk' % (HOST, project)
local_branch = os.path.join(REPO_BASE_DIR, project, branch_name)
local_tree = os.path.join(WORKINGTREE_BASE_DIR, project)

# Make sure base directory exists
if not os.path.exists(REPO_BASE_DIR):
    os.makedirs(REPO_BASE_DIR)
if not os.path.exists(WORKINGTREE_BASE_DIR):
    os.makedirs(WORKINGTREE_BASE_DIR)

# Set up config for base directory
config = LocationConfig(REPO_BASE_DIR)
config.set_user_option(
    'push_location', 'bzr+ssh://%s/bzr/%s' % (HOST, USER),
    STORE_LOCATION_APPENDPATH)
config.set_user_option(
    'public_branch', 'http://%s/%s' % (HOST, USER),
    STORE_LOCATION_APPENDPATH)

# Create repository, without trees
repo_dir = os.path.join(REPO_BASE_DIR, project)
if not os.path.exists(repo_dir):
    subprocess.check_call(
        ['bzr', 'init-repo', '--no-trees', '--format=rich-root-pack', repo_dir])

config = LocationConfig(repo_dir)
config.set_user_option('submit_branch', remote_branch)

# Make the remote repository
sys.stderr.write('Creating remote repository for %s\n' % project)
subprocess.check_call(['ssh', '%s@%s' % (USER, HOST),
                       'bzr', 'make-module-repo', project])

# branch the code.
sys.stderr.write('Branching %s to %s\n' % (remote_branch, local_branch))
subprocess.check_call(['bzr', 'branch', remote_branch, local_branch])

# Create a working tree if none exists yet.
if os.path.exists(local_tree):
    # Tell the user to switch
    sys.stderr.write(
        "Working tree already exists.  You can switch to the new branch with:\n"
        "  bzr switch %s" % branch_name)
else:
    sys.stderr.write('Creating working tree\n')
    subprocess.check_call(['bzr', 'checkout', '--lightweight',
                           local_branch, local_tree])
