# Simple script for mass-registering all the branches in the GNOME Bzr mirror
# on Launchpad.
# Copyright (C) 2008 Jelmer Vernooij <jelmer@samba.org>
# GPLv3+

# TODO: Set registrant to gnome-bzr-mirror 
#       (Launchpads API doesn't appear to allow this atm)
# TODO: Mark removed branches as "Abandoned"/"Merged" in Launchpad

from bzrlib import urlutils
from bzrlib.bzrdir import BzrDir
from bzrlib.commands import register_command, Command
from bzrlib.directory_service import directories
from bzrlib.option import Option
from bzrlib.plugin import load_plugins
from bzrlib.transport import get_transport, do_catching_redirections

from xmlrpclib import Fault
import os

load_plugins()

from bzrlib.plugins.launchpad.lp_registration import (
        LaunchpadService, BranchRegistrationRequest, BranchBugLinkRequest)

from bzrlib.plugins.bzrtools.bzrtools import iter_branch_tree, apache_ls, list_branches

def register_branch(service, product, lp_product, branch, lp_branch):
    """Register a branch on Launchpad.

    :param service: Launchpad service handle
    :param product: Product name in GNOME Subversion
    :param lp_product: Product name in Launchpad
    :param branch: Branch name in GNOME Subversion
    :param lp_branch: Branch name to use in Launchpad
    """
    public_url = urlutils.join(PUBLIC_BASE_URL, "%s/%s" % (product, branch))

    rego = BranchRegistrationRequest(branch_url=public_url, 
                                     branch_name=lp_branch,
                                     product_name=lp_product)
    try:
        rego.submit(service)
    except Fault, e:
        if e.faultCode == 50:
            # Already registered, ignore
            print "Skipping %s/%s, %s" % (lp_product, lp_branch, e.faultString)
            pass
        else:
            print "Error occurred: %r" % e
    else:
        print 'Branch %s/%s registered. (%s)' % (lp_product, lp_branch, public_url)

# Private URL used for accessing the branches. 
# Fastest, but perhaps not the most public way of accessing the branches
PRIVATE_BASE_URL = "http://bzr-mirror.gnome.org/"

# Public URL that's used as base when sending URLs to Launchpad
PUBLIC_BASE_URL = "http://bzr-mirror.gnome.org/"

# Name of the registrant in Launchpad.
REGISTRANT_NAME = "gnome-bzr-mirror"

# Some products have different names in GNOME Subversion and Launchpad
# This map maps from GNOME Subversion names to Launchpad names.
PRODUCT_MAP = {
        "NetworkManager": "network-manager"
}

# Connect to Launchpad
service = LaunchpadService()
service.gather_user_credentials()

t = get_transport(PRIVATE_BASE_URL)

def transport_list(t):
    if t.base.startswith("http://"):
        return apache_ls(t)
    return t.list_dir(".")

for product in transport_list(t):
    product_t = t.clone(product)
    lp_product = PRODUCT_MAP.get(product, product)

    children = transport_list(product_t)

    if "trunk" in children:
        register_branch(service, product, lp_product, "trunk", "trunk")

    if "branches" in children:
        branches_t = product_t.clone("branches")
        for branch_name in transport_list(branches_t):
            register_branch(service, product, lp_product, 
                            "branches/%s" % branch_name, branch_name)



