#!/bin/sh
#
# Script to convert an rsync'd copy of a GNOME SVN module into a svnsync-able mirror
#

MODULE=$1

echo $MODULE

cd /srv/svn/$MODULE

FROM=http://svn.gnome.org/svn/$MODULE
TO=file:///srv/svn/$MODULE

mv hooks hooks_old
mkdir hooks

echo '#!/bin/sh' > hooks/pre-revprop-change
chmod +x hooks/pre-revprop-change
    
svn propset svn:sync-from-uuid --revprop -r 0 "$(cat db/uuid)" $TO
svn propset svn:sync-last-merged-rev --revprop -r 0 "$(($(ls db/revs | wc -w)-1))" $TO
svn propset svn:sync-from-url --revprop -r 0 "$FROM" $TO

svnsync --non-interactive sync $TO
