#!/usr/bin/env python
# This script is a cronjob to automatically remove things from the mirror
# when they are no longer on svn.gnome.org

import sys, os
import httplib
from xml.dom.minidom import parseString
from sets import Set

# lets get a list of current modules
conn = httplib.HTTPConnection("svn.gnome.org")
conn.request("GET", "/svn/")
r = conn.getresponse()
if r.status != 200:
    print r.status
    sys.exit(1)
modules_str = r.read()
conn.close()

# and now as a list
doc = parseString(modules_str)
remote = Set([str(x.getAttribute("name")) for x in doc.getElementsByTagName("dir")])
remote = remote - Set(["bin", "tmp"])

# now lets get a list of modules we have locally
locally = Set([x for x in os.listdir('/srv/svn') if os.path.isdir(os.path.join('/srv/svn/',x))])

for x in locally - remote:
    print "rm: ", x
    os.system("rm -rf /src/hg/%s" % x)
    os.system("rm -rf /srv/git/%s" % x)
    os.system("rm -rf /srv/bzr/%s" % x)
    os.system("rm -rf /srv/svn/%s" % x)

for x in remote - locally:
    print "create: ", x
    os.system("./svn-create.sh %s" % x)
    os.system("./bzr-create.sh %s" % x)
    os.system("./git-create.sh %s" % x)
    os.system("./hg-create.sh %s" % x)
